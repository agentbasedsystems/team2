############################################
###### Before running the simulations ######
############################################

Ensure that map.txt and xploration.properties are present in the project root folder (same level as src/ folder)



#####################################
###### Running the simulations ######
#####################################

In order to run Team 2 version of the platform, pass following parameters to the programm:

    -gui -agents RegDesk:org.xploration.team2.platform.RegistrationDesk;TerrainSim:org.xploration.team2.platform.TerrainSimulator;Radio:org.xploration.team2.platform.Radio;MovementSim:org.xploration.team2.platform.MovementSimulator

Above command ensures that all platform-related agents are started automatically.


After that, you will have to add team company agents manually using JADE GUI.




########################################
###### After simulation has ended ######
########################################

After simulation run has ended, you will see final scores printed in the console
AND a text file called xploration-results.txt being created/updated in the same folder as
map.txt and xploration.properties with a copy of results.