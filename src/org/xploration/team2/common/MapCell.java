package org.xploration.team2.common;

import org.xploration.ontology.Cell;

import java.awt.*;

/**
 * Created by Ivan on 27/04/2017.
 */
public class MapCell extends SimulationObject {

    protected String mineral;

    public MapCell(Point location) {
        super(location);
    }

    public MapCell(Point location, String mineral) {
        this(location);
        this.mineral = mineral;
    }

    public MapCell(Cell cell) {
        this(new Point(cell.getX(), cell.getY()), cell.getMineral());
    }

    public String getMineral() {
        return mineral;
    }

    public void setMineral(String mineral) {
        this.mineral = mineral;
    }

    public Cell getOntologyCell() {
        Cell cell = new Cell();
        cell.setX(((Double)location.getX()).intValue());
        cell.setY(((Double)location.getY()).intValue());
        cell.setMineral(mineral);

        return cell;
    }

    // used for sharing analysed but unclaimed cells
    public Cell getFaultyOntologyCell() {
        Cell cell = new Cell();
        cell.setX(((Double)location.getX()).intValue());
        cell.setY(((Double)location.getY()).intValue());
        cell.setMineral(mineral.charAt(0) == 'E' ? "A" : String.valueOf((char)(mineral.charAt(0)+1)));
        return cell;
    }

    public String toString() {
        return "[ " + location.toString() + " => " + mineral + " ]";
    }
}
