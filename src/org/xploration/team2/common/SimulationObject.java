package org.xploration.team2.common;

import java.awt.*;

/**
 * Created by Ivan on 27/04/2017.
 */
public abstract class SimulationObject {
    protected Point location;

    public SimulationObject(Point location) {
        this.location = location;
    }

    public Point getLocation() {
        return location;
    }

    public void setLocation(Point location) {
        this.location = location;
    }
}
