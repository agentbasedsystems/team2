package org.xploration.team2.common;

/**
 * Created by Ivan on 13/05/2017.
 */
public enum Direction {
    NORTH, NORTH_EAST, SOUTH_EAST, SOUTH, SOUTH_WEST, NORTH_WEST
}
