package org.xploration.team2.common;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by Ivan on 13/05/2017.
 */
public abstract class AsteroidMap {
    protected int n; //Dimensions on the X axis (height)
    protected int m; //Dimensions on the Y axis (width)
    protected Map<Point, MapCell> map;

    public AsteroidMap() {
        map = new HashMap<Point, MapCell>();
    }

    public MapCell getCell(Point location) {
        if (map.containsKey(location)) {
            return map.get(location);
        }
        return null;
    }

    public String getMineralAt(Point location) {
        if (map.containsKey(location)) {
            return getCell(location).getMineral();
        }
        return null;
    }

    public boolean isValidLocation(Point location) {
        return map.containsKey(location);
    }

    public Dimension getDimension() {
        return new Dimension(m, n); //M is width and N is height
    }

    public Set<Point> getExistingPoints() {
        return map.keySet();
    }

    public Point coordinateInDirection(Point from, Direction direction) {
        Point newCoordinate = new Point(from);
        switch (direction) {
            case NORTH:
                newCoordinate.translate(-2, 0);
                break;
            case NORTH_EAST:
                newCoordinate.translate(-1, 1);
                break;
            case SOUTH_EAST:
                newCoordinate.translate(1, 1);
                break;
            case SOUTH:
                newCoordinate.translate(2, 0);
                break;
            case SOUTH_WEST:
                newCoordinate.translate(1, -1);
                break;
            case NORTH_WEST:
                newCoordinate.translate(-1, -1);
                break;
        }

        if (newCoordinate.x <= 0 || newCoordinate.x > n) {
            newCoordinate.x = Math.abs(Math.abs(newCoordinate.x) - n);
        }

        if (newCoordinate.y <= 0 || newCoordinate.y > m) {
            newCoordinate.y = Math.abs(Math.abs(newCoordinate.y) - m);
        }

        return newCoordinate;
    }

    public Boolean isValidMovement(Point from, Point to) {
        return to.equals(coordinateInDirection(from, Direction.NORTH)) ||
                to.equals(coordinateInDirection(from, Direction.NORTH_EAST)) ||
                to.equals(coordinateInDirection(from, Direction.NORTH_WEST)) ||
                to.equals(coordinateInDirection(from, Direction.SOUTH)) ||
                to.equals(coordinateInDirection(from, Direction.SOUTH_EAST)) ||
                to.equals(coordinateInDirection(from, Direction.SOUTH_WEST));
    }

    public int distance(Point from, Point to) {
        int eastD = (m + to.y - from.y) % m;
        int westD = (m + from.y - to.y) % m;
        int northD = (n + from.x - to.x) % n;
        int southD = (n + to.x - from.x) % n;

        int distY = Math.min(eastD, westD);
        int distX = Math.min(northD, southD);

        return distY + Math.max(0, (distX - distY) / 2);
    }
    
    public ArrayList getListOfPointsInRange(int range, Point origin){
        ArrayList listOfPointsInRange = new ArrayList();
        for (Map.Entry<Point, MapCell> point: map.entrySet()){
            if (range >= distance(point.getKey(),origin)) {
                listOfPointsInRange.add(point.getKey());
            }
        }
        return listOfPointsInRange;
    }

    public ArrayList getListOfPointsInTheExactlyRange(int range, Point origin){
        ArrayList listOfPointsInRange = new ArrayList();
        for (Map.Entry<Point, MapCell> point: map.entrySet()){
            if (range == distance(point.getKey(),origin)) {
                listOfPointsInRange.add(point.getKey());
            }
        }

        //Purge edges, we are not going to use them.
        ArrayList listOfEdgePoints = new ArrayList();
        listOfEdgePoints.add(origin);
        listOfEdgePoints.add(origin);
        listOfEdgePoints.add(origin);
        listOfEdgePoints.add(origin);
        listOfEdgePoints.add(origin);
        listOfEdgePoints.add(origin);


        for (int i = 0;i<range;i++){
            listOfEdgePoints.set(0, coordinateInDirection((Point) listOfEdgePoints.get(0), Direction.NORTH));
        }
        for (int i = 0;i<range;i++){
            listOfEdgePoints.set(1, coordinateInDirection((Point) listOfEdgePoints.get(1),Direction.NORTH_EAST));
        }
        for (int i = 0;i<range;i++){
            listOfEdgePoints.set(2, coordinateInDirection((Point) listOfEdgePoints.get(2),Direction.NORTH_WEST));
        }
        for (int i = 0;i<range;i++){
            listOfEdgePoints.set(3, coordinateInDirection((Point) listOfEdgePoints.get(3),Direction.SOUTH));
        }
        for (int i = 0;i<range;i++){
            listOfEdgePoints.set(4, coordinateInDirection((Point) listOfEdgePoints.get(4),Direction.SOUTH_EAST));
        }
        for (int i = 0;i<range;i++){
            listOfEdgePoints.set(5, coordinateInDirection((Point) listOfEdgePoints.get(5),Direction.SOUTH_WEST));
        }
        for (Object point : listOfEdgePoints){
            if (listOfPointsInRange.contains(point)){
                listOfPointsInRange.remove(listOfPointsInRange.indexOf(point));
            }
        }
        return listOfPointsInRange;
    }
}
