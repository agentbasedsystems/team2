package org.xploration.team2.common;

import jade.content.AgentAction;
import jade.content.ContentManager;
import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.AID;
import jade.lang.acl.ACLMessage;
import org.xploration.ontology.XplorationOntology;

/**
 * Created by Ivan on 27/04/2017.
 */
public class MessageBuilder {

    private static Codec codec = new SLCodec();
    private static Ontology ontology = XplorationOntology.getInstance();
    private static ContentManager cm = new ContentManager();

    static {
        cm.registerLanguage(codec);
        cm.registerOntology(ontology);
    }

    public static ACLMessage getMessage(AID toAgent, int performative, String protocol) {
        ACLMessage msg = new ACLMessage(performative);
        msg.addReceiver(toAgent);
        msg.setLanguage(codec.getName());
        msg.setOntology(ontology.getName());
        msg.setProtocol(protocol);
        return msg;
    }

    public static ACLMessage getActionMessage(AID toAgent, int performative, AgentAction action, String protocol) throws Codec.CodecException, OntologyException {
        ACLMessage msg = getMessage(toAgent, performative, protocol);

        Action agAction = new Action(toAgent, action);
        cm.fillContent(msg, agAction);
    
        return msg;
    }

    public static ACLMessage getResponseMessage(ACLMessage original, int performative) {
        ACLMessage msg = original.createReply();
        msg.setPerformative(performative);

        return msg;
    }

    public static ACLMessage getResponseActionMessage(ACLMessage original, int performative, AgentAction action) throws Codec.CodecException, OntologyException {
        ACLMessage msg = original.createReply();
        msg.setPerformative(performative);

        Action agAction = new Action(original.getSender(), action);
        cm.fillContent(msg, agAction);

        return msg;
    }
}
