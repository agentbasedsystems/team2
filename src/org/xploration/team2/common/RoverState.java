package org.xploration.team2.common;

/**
 * Created by Ivan on 15/05/2017.
 */
public enum RoverState {
    IDLE, MOVING, ANALYZING
}
