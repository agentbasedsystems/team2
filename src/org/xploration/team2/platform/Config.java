package org.xploration.team2.platform;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * Created by Steven on 27/05/2017.
 */
public class Config {
    private static Config instance;

    private Properties properties;
    private InputStream input;

    public static Config getInstance() {
        if(instance == null) {
            instance = new Config();
        }
        return instance;
    }

    public Config() {
        try {
            Path path = Paths.get(Constants.FILENAME);
            input = Files.newInputStream(path);
            properties = new Properties();
            properties.load(input);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public String getProperty(String key){
        String result = null;
        if(key != null && !key.trim().isEmpty()) {
            result = this.properties.getProperty(key);
        }
        return result;
    }

    public Integer getPropertyAsInt(String key) {
        Integer result = null;
        try {
            String value = getProperty(key);
            result = Integer.valueOf(value);
        } catch(Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
