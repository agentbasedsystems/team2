package org.xploration.team2.platform.simulation;

import jade.core.AID;
import org.xploration.ontology.Cell;
import org.xploration.ontology.Team;

/**
 * Created by Ivan on 27/04/2017.
 */
public class Rover extends SimulationAgent {
    public Rover(AID agentId, Team team, Cell cell) {
        super(agentId, team, cell);
    }
}
