package org.xploration.team2.platform.simulation;

import jade.core.AID;
import org.xploration.ontology.Cell;
import org.xploration.ontology.Team;
import org.xploration.team2.platform.Constants;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ivan on 27/04/2017.
 */
public class Simulation {

    public static Simulation instance;

    private Map<AID, Capsule> capsules;
    private Map<AID, Rover> rovers;
    private SimulationMap map;
    public boolean allAgentsRegistered = false;

    public static synchronized Simulation getInstance() {
        if (instance == null) {
            instance = new Simulation();
        }
        return instance;
    }

    public Simulation() {
        capsules = new HashMap<AID, Capsule>();
        rovers = new HashMap<AID, Rover>();
        map = new SimulationMap();
        map.printMap();
    }

    public Capsule createCapsule(AID agentId, Team team, Cell cell) {
        Capsule capsule = new Capsule(agentId, team, cell);
        capsules.put(capsule.getAgentId(), capsule);
        return capsule;
    }

    public Rover createRover(AID agentId, Team team, Cell cell) {
        Rover rover = new Rover(agentId, team, cell);
        rovers.put(rover.getAgentId(), rover);
        return rover;
    }

    public Capsule getCapsule(AID aId) {
        if (capsules.containsKey(aId)) {
            return capsules.get(aId);
        }
        return null;
    }

    public Capsule getCapsule(Team team) {
        for (Capsule capsule : capsules.values()) {
            if (capsule.getTeam().getTeamId() == team.getTeamId()) {
                return capsule;
            }
        }
        return null;
    }

    public Rover getRover(AID aId) {
        if (rovers.containsKey(aId)) {
            return rovers.get(aId);
        }
        return null;
    }

    public SimulationMap getMap() {
        return map;
    }

    public boolean isValidLocation(Point location) {
        return map.isValidLocation(location);
    }

    public boolean isValidMovement(Point from, Point to) {
        return map.isValidMovement(from, to);
    }

    public Map<AID, SimulationAgent> getAgentsInSignalRange(Point location) {
        Map<AID, SimulationAgent> result = new HashMap<AID, SimulationAgent>();
        for(Rover rover : rovers.values()) {
            if(map.distance(location, rover.getLocation()) <= Constants.SIGNAL_RANGE) {
                result.put(rover.getAgentId(), rover);
            }
        }
        for(Capsule capsule : capsules.values()) {
            if(map.distance(location, capsule.getLocation()) <= Constants.SIGNAL_RANGE) {
                result.put(capsule.getAgentId(), capsule);
            }
        }
        return result;
    }

    public boolean areWithinRange(SimulationAgent ag1, SimulationAgent ag2) {
        return map.distance(ag1.getLocation(), ag2.getLocation()) <= Constants.SIGNAL_RANGE;
    }

}
