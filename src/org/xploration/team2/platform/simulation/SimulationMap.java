package org.xploration.team2.platform.simulation;

import org.xploration.team2.common.AsteroidMap;
import org.xploration.team2.common.MapCell;

import java.awt.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Ivan on 27/04/2017.
 */
public class SimulationMap extends AsteroidMap {

    public SimulationMap() {
        super();
        loadMinerals();
    }

    private void loadMinerals() {
        String fileName = "map.txt";

        try {
            System.out.println(Paths.get(fileName));
            List<String> lines = Files.readAllLines(Paths.get(fileName));

            int i = 0;
            for (String line : lines) {
                if (i == 0) {
                    Pattern p = Pattern.compile("^\\((\\d+),(\\d+)\\)$");
                    Matcher matcher = p.matcher(line);
                    if (matcher.matches()) {
                        n = Integer.valueOf(matcher.group(1));
                        m = Integer.valueOf(matcher.group(2));
                    } else {
                        throw new IllegalArgumentException("Invalid map.txt format");
                    }
                } else {
                    int j = 1;
                    for (String mineral : line.split(" ")) {
                        Point location;
                        if ((i % 2) == 0) {
                            location = new Point(i, (j * 2));
                        } else {
                            location = new Point(i, (j * 2) - 1);
                        }
                        map.put(location, new MapCell(location, mineral));
                        j++;
                    }
                }
                i++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Point getRandomLocation() {
        Point location;
        do {
            Random rand = new Random();
            int x = rand.nextInt(n+1);
            int y = rand.nextInt(m+1);
            location = new Point(x, y);
        } while (!map.keySet().contains(location));
        return location;
    }

    public void printMap() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                Point p = new Point(i+1, j+1);
                if (map.keySet().contains(p)) {
                    sb.append(map.get(p).getMineral());
                } else {
                    sb.append(" ");
                }
            }
            sb.append("\r\n");
        }
        System.out.println(sb.toString());
    }

}
