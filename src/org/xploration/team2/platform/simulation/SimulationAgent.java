package org.xploration.team2.platform.simulation;

import jade.core.AID;
import org.xploration.ontology.Cell;
import org.xploration.ontology.Team;
import org.xploration.team2.common.SimulationObject;

import java.awt.*;

/**
 * Created by Ivan on 09/05/2017.
 */
public class SimulationAgent extends SimulationObject {
    protected Team team;
    protected AID agentId;

    public SimulationAgent(AID agentId, Team team, Cell cell) {
        super(new Point(cell.getX(), cell.getY()));
        this.team = team;
        this.agentId = agentId;
    }

    public Team getTeam() {
        return team;
    }

    public AID getAgentId() {
        return agentId;
    }
}
