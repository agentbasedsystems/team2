package org.xploration.team2.platform.simulation;

import org.xploration.ontology.Cell;
import org.xploration.ontology.ClaimCellInfo;
import org.xploration.ontology.Map;
import org.xploration.ontology.Team;

import java.awt.*;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Steven on 27/05/2017.
 * A.K.A. Scorer / Counter / Referee / Arbiter / Fourth Official
 */

public class ScoreTable {
    private static final int POINTS_FIRST_CLAIM = 1;
    private static final int POINTS_FALSE_CLAIM = -5;

    private HashMap<Point, List<Claim>> claimTable;
    private HashMap<Integer, List<Claim>> falseClaimsPerTeam;
    private HashMap<Point, Claim> firstClaims;
    private HashMap<Integer, Integer> subScorePerTeam;
    private SimulationMap map;
    private LocalDateTime asteroidEpoch;

    public ScoreTable(List<Team> teams, SimulationMap map) {
        asteroidEpoch = LocalDateTime.now();
        claimTable = new HashMap<>();
        falseClaimsPerTeam = new HashMap<>();
        firstClaims = new HashMap<>();
        subScorePerTeam = new HashMap<>();
        for(Point point : map.getExistingPoints()) {
            claimTable.put(point, new ArrayList<>());
        }
        for(Team team : teams) {
            falseClaimsPerTeam.put(team.getTeamId(), new ArrayList<>());
            subScorePerTeam.put(team.getTeamId(), 0);
        }
        this.map = map;
    }

    public void doClaim(ClaimCellInfo claimInfo) {
        Team team = claimInfo.getTeam();
        Map claimedMap = claimInfo.getMap();
        for(Iterator<Cell> iterator = claimedMap.getAllCellList(); iterator.hasNext(); ) {
            Cell cell = iterator.next();
            Point point = new Point(cell.getX(), cell.getY());
            Claim claim = new Claim(team, cell.getMineral());
            updateTables(point, claim);
        }
    }

    public Integer getScoreForTeam(Integer team) {
        return subScorePerTeam.get(team) * firstClaims.size();
    }

    public Integer getNumberOfFirstClaimsForTeam(Integer team) {
        Integer result = 0;
        for(Point p : firstClaims.keySet()) {
            Claim c = firstClaims.get(p);
            if(c.team.getTeamId() == team) {
                result ++;
            }
        }
        return result;
    }

    public Integer getNumberOfFalseClaimsForTeam(Integer team) {
        return falseClaimsPerTeam.get(team).size();
    }

    public Integer getNumberOfLateClaimsForTeam(Integer team) {
        Integer result = 0;
        for(Point p : claimTable.keySet()) {
            List<Claim> claims = claimTable.get(p);
            for(Integer i = 1; i < claims.size(); i++) {
                Claim c = claims.get(i);
                if(c.team.getTeamId() == team) {
                    result ++;
                }
            }

        }
        return result;
    }

    public Integer getTotalNumberOfOwnedCells() {
        return firstClaims.size();
    }

    public List<String> getClaimsForPoint(Point p) {
        ArrayList<String> result = new ArrayList<>();
        for(Claim c : claimTable.get(p)) {
            result.add(c.toString());
        }
        return result;
    }

    public LocalDateTime getEpoch() {
        return asteroidEpoch;
    }

    private void updateTables(Point point, Claim claim) {
        Integer team = claim.getTeam().getTeamId();
        String mineral = map.getMineralAt(point);
        if(claim.mineral.equals(mineral)) {
            // correct claim
            if(!firstClaims.containsKey(point)) {
                firstClaims.put(point, claim);
                subScorePerTeam.put(team, subScorePerTeam.get(team) + POINTS_FIRST_CLAIM);
            }
        } else {
            // false claim
            if(!falseClaimsPerTeam.containsKey(team)) {
                falseClaimsPerTeam.put(team, new ArrayList<>());
            }
            falseClaimsPerTeam.get(team).add(claim);
            subScorePerTeam.put(team, subScorePerTeam.get(team) + POINTS_FALSE_CLAIM);
        }
        claimTable.get(point).add(claim);
    }

    private class Claim {
        private LocalDateTime time;
        private Team team;
        private String mineral;

        public Claim(Team team, String mineral) {
            this.time = LocalDateTime.now();
            this.team = team;
            this.mineral = mineral;
        }

        public Team getTeam() {
            return team;
        }

        public String toString() {
            long hours = ChronoUnit.HOURS.between(asteroidEpoch, time);
            long minutes = ChronoUnit.MINUTES.between(asteroidEpoch, time);
            long seconds = ChronoUnit.SECONDS.between(asteroidEpoch, time);
            long millis = ChronoUnit.MILLIS.between(asteroidEpoch, time);
            //return hours+"%:"+minutes+":"+seconds+"."+millis+": Team "+team.getTeamId()+" -> "+mineral;
            return String.format("%02d:%02d:%02d.%04d: Team %s -> %s", hours, minutes, seconds, millis, team.getTeamId(), mineral);
        }
    }
}
