package org.xploration.team2.platform;

import jade.content.Concept;
import jade.content.ContentElement;
import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.*;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.domain.JADEAgentManagement.JADEManagementOntology;
import jade.domain.JADEAgentManagement.ShutdownPlatform;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import jade.wrapper.StaleProxyException;
import org.xploration.ontology.*;
import org.xploration.team2.common.MessageBuilder;
import org.xploration.team2.platform.simulation.ScoreTable;
import org.xploration.team2.platform.simulation.Simulation;
import org.xploration.team2.platform.simulation.SimulationAgent;
import org.xploration.team2.platform.simulation.SimulationMap;

import java.awt.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Ivan on 08/05/2017.
 */
public class SpaceCraft extends Agent {

    private Codec codec = new SLCodec();
    private Ontology ontology = XplorationOntology.getInstance();
    private List<Team> registeredTeams;
    private Simulation simulation;
    private ScoreTable scoreTable;
    private ThreadedBehaviourFactory tbf;
    private Behaviour claimListenThreadedBehaviour;


    protected void setup() {
        System.out.println(getLocalName()+": has entered into the system");

        //Register of the codec and the ontology to be used in the ContentManager
        getContentManager().registerLanguage(codec);
        getContentManager().registerOntology(ontology);

        simulation = Simulation.getInstance();

        registeredTeams = new LinkedList<Team>();
        Object[] args = getArguments();
        Object[] teams = (Object[])args[0];
        for (Object team : teams) {
            registeredTeams.add((Team) team);
        }

        try {
            // Creates its own description
            DFAgentDescription dfd = new DFAgentDescription();
            ServiceDescription sdCapsule = new ServiceDescription();
            sdCapsule.setName(this.getName());
            sdCapsule.setType(XplorationOntology.CAPSULEREGISTRATIONSERVICE);
            dfd.addServices(sdCapsule);
            ServiceDescription sdRover = new ServiceDescription();
            sdRover.setName(this.getName());
            sdRover.setType(XplorationOntology.ROVERREGISTRATIONSERVICE);
            dfd.addServices(sdRover);
            ServiceDescription sdClaim = new ServiceDescription();
            sdClaim.setName(this.getName());
            sdClaim.setType(XplorationOntology.SPACECRAFTCLAIMSERVICE);
            dfd.addServices(sdClaim);
            // Registers its description in the DF
            DFService.register(this, dfd);
            System.out.println(getLocalName()+": registered in the DF as CAPSULE/ROVER_AGENT_REGISTRATION");
        } catch (FIPAException e) {
            e.printStackTrace();
        }

        addBehaviour(getReleaseCapsulesBehaviour());

        int expectingAgents = registeredTeams.size() * 2;
        addBehaviour(getAgentRegistrationBehaviour(expectingAgents));

        tbf = new ThreadedBehaviourFactory();
        claimListenThreadedBehaviour = getClaimListenThreadedBehaviour();
        addBehaviour(claimListenThreadedBehaviour);
        addBehaviour(getMissionEndBehaviour());
    }

    private Behaviour getAgentRegistrationBehaviour(int expectingAgents) {
        return new SimpleBehaviour(this) {
            private int agentsRegistered = 0;

            @Override
            public void action() {
                // Waits for agent registration notification
                ACLMessage msg = blockingReceive(
                        MessageTemplate.and(
                                MessageTemplate.or(
                                        MessageTemplate.MatchProtocol(XplorationOntology.CAPSULEREGISTRATIONINFO),
                                        MessageTemplate.MatchProtocol(XplorationOntology.ROVERREGISTRATIONINFO)
                                ),
                                MessageTemplate.and(
                                        MessageTemplate.MatchLanguage(codec.getName()),
                                        MessageTemplate.and(
                                                MessageTemplate.MatchOntology(ontology.getName()),
                                                MessageTemplate.MatchPerformative(ACLMessage.INFORM)
                                        )
                                )

                        )
                );
                if (msg != null) {
                    ContentElement ce;
                    try {
                        ce = getContentManager().extractContent(msg);

                        // We expect an action inside the message
                        if (ce instanceof Action) {
                            Action agAction = (Action) ce;
                            Concept conc = agAction.getAction();

                            if (conc instanceof CapsuleRegistrationInfo) {
                                AID fromAgent = msg.getSender();
                                Team requestorTeam = ((CapsuleRegistrationInfo) conc).getTeam();
                                Cell location = ((CapsuleRegistrationInfo) conc).getCell();
                                simulation.createCapsule(fromAgent, requestorTeam, location);
                                agentsRegistered++;
                                System.out.println(myAgent.getLocalName()+": Capsule registered for team "+requestorTeam.getTeamId());
                            } else if (conc instanceof RoverRegistrationInfo) {
                                AID fromAgent = msg.getSender();
                                Team requestorTeam = ((RoverRegistrationInfo) conc).getTeam();
                                Cell location = ((RoverRegistrationInfo) conc).getCell();
                                simulation.createRover(fromAgent, requestorTeam, location);
                                agentsRegistered++;
                                System.out.println(myAgent.getLocalName()+": Rover registered for team "+requestorTeam.getTeamId());
                            } else {
                                ACLMessage reply = MessageBuilder.getResponseMessage(msg, ACLMessage.NOT_UNDERSTOOD);
                                myAgent.send(reply);
                                System.out.println(myAgent.getLocalName()+": replied with NOT UNDERSTOOD.\tMessage: "+msg.getContent());
                            }
                        }
                    } catch (Codec.CodecException | OntologyException e) {
                        e.printStackTrace();

                        ACLMessage reply = MessageBuilder.getResponseMessage(msg, ACLMessage.NOT_UNDERSTOOD);
                        myAgent.send(reply);
                        System.out.println(myAgent.getLocalName()+": NOT UNDERSTOOD sent");
                    }
                }
            }

            @Override
            public boolean done() {
                System.out.println(myAgent.getLocalName()+": AGENT REGISTRATION ENDED?: " + (agentsRegistered == expectingAgents));
                if (agentsRegistered == expectingAgents) {
                    simulation.allAgentsRegistered = true;
                }
                return agentsRegistered == expectingAgents;
            }
        };
    }

    private Behaviour getReleaseCapsulesBehaviour() {
        return new OneShotBehaviour(this) {
            @Override
            public void action() {
                // releasing capsules
                System.out.println("######################################################");
                System.out.println("######################################################");
                System.out.println("##############                         ###############");
                System.out.println("##############    Releasing Capsules   ###############");
                System.out.println("##############                         ###############");
                System.out.println("######################################################");
                System.out.println("######################################################");

                HashSet<Point> dropOffs = new HashSet<>();
                SimulationMap map = simulation.getMap();
                scoreTable = new ScoreTable(registeredTeams, map);

                for(Team team : registeredTeams) {
                    // get random dropoff Point
                    Point dropOff;
                    do {
                        dropOff = map.getRandomLocation();
                    } while (dropOffs.contains(dropOff));
                    dropOffs.add(dropOff);

                    try {
                        // create capsule agent
                        System.out.println(myAgent.getLocalName() + ": Attempting to create Capsule agent for team " + team.getTeamId());
                        System.out.println(myAgent.getLocalName() + ": Dropoff location: " + dropOff);
                        ContainerController cc = getContainerController();
                        Object params[] = new Object[6];
                        params[0] = ((Double)dropOff.getX()).intValue();
                        params[1] = ((Double)dropOff.getY()).intValue();
                        params[2] = ((Double)map.getDimension().getWidth()).intValue();
                        params[3] = ((Double)map.getDimension().getHeight()).intValue();
                        params[4] = Constants.MISSION_LENGTH/1000;
                        params[5] = Constants.SIGNAL_RANGE;
                        AgentController ac = cc.createNewAgent("Capsule" + team.getTeamId(), "org.xploration.team" + team.getTeamId() + ".company.Capsule" + team.getTeamId(), params);
                        ac.start();
                    } catch (StaleProxyException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
    }

    private Behaviour getClaimListenThreadedBehaviour() {
        return tbf.wrap(new CyclicBehaviour() {
            @Override
            public void action() {
                // Waits for broadcasts
                ACLMessage msg = receive(
                        MessageTemplate.and(
                                MessageTemplate.MatchProtocol(XplorationOntology.CLAIMCELLINFO),
                                MessageTemplate.and(
                                        MessageTemplate.MatchLanguage(codec.getName()),
                                        MessageTemplate.and(
                                                MessageTemplate.MatchOntology(ontology.getName()),
                                                MessageTemplate.MatchPerformative(ACLMessage.INFORM)
                                        )
                                )
                        )
                );
                if (msg != null) {

                    ContentElement ce;
                    try {
                        ce = getContentManager().extractContent(msg);

                        if (ce instanceof Action) {
                            Action agAction = (Action) ce;
                            Concept conc = agAction.getAction();

                            if (conc instanceof ClaimCellInfo) {

                                AID fromAgentAID = msg.getSender();
                                SimulationAgent fromAgent = simulation.getCapsule(fromAgentAID);

                                ClaimCellInfo claimInfo = (ClaimCellInfo) conc;
                                Team targetTeam = claimInfo.getTeam();

                                System.out.println(myAgent.getLocalName()+": received cell claim from "+(msg.getSender()).getLocalName()+" at "+fromAgent.getLocation().toString());

                                if (fromAgent.getTeam().getTeamId() == targetTeam.getTeamId()) {
                                    System.out.println(myAgent.getLocalName()+": Valid TeamID - OK");
                                    scoreTable.doClaim(claimInfo);
                                    for(Team team : registeredTeams) {
                                        System.out.println(myAgent.getLocalName()+": SCORES | Team "+team.getTeamId()+": "+scoreTable.getScoreForTeam(team.getTeamId()));
                                    }
                                }
                            }
                        }
                    } catch (Codec.CodecException | OntologyException e) {
                        e.printStackTrace();
                        System.out.println(myAgent.getLocalName()+": INVALID CELL CLAIM INFORM!");
                    }
                } else {
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private Behaviour getMissionEndBehaviour() {
        return new WakerBehaviour(this, Constants.MISSION_LENGTH) {
            @Override
            public void onWake() {
                removeBehaviour(claimListenThreadedBehaviour);

                System.out.println();
                System.out.println("###################################################");
                System.out.println("###################################################");
                System.out.println("##############                      ###############");
                System.out.println("##############      Final Score     ###############");
                System.out.println("##############                      ###############");
                System.out.println("###################################################");
                System.out.println("###################################################");
                System.out.println();
                System.out.println("TOTAL # CELLS OWNED: " + scoreTable.getTotalNumberOfOwnedCells());
                System.out.println("===================================================\n");


                for(Team team : registeredTeams) {
                    Integer teamId = team.getTeamId();
                    System.out.println("TEAM "+teamId+"\n");

                    System.out.println("Cells owned: " + scoreTable.getNumberOfFirstClaimsForTeam(teamId));
                    System.out.println("False claims: " + scoreTable.getNumberOfFalseClaimsForTeam(teamId));
                    System.out.println("Late claims: " + scoreTable.getNumberOfLateClaimsForTeam(teamId)+"\n");

                    System.out.println("TOTAL SCORE: " + scoreTable.getScoreForTeam(teamId));
                    System.out.println("---------------------------------------------------\n");
                }

                List<String> lines = new ArrayList<>();
                lines.add("Xploration - started at "+scoreTable.getEpoch()+" finished at "+ LocalDateTime.now().toString()+"\n");
                for(Point p : simulation.getMap().getExistingPoints()) {
                    String lineStart = "["+p.getX()+","+p.getY()+"] <== {";
                    List<String> line = new ArrayList<>();
                    line.addAll(scoreTable.getClaimsForPoint(p));
                    lines.add(lineStart + String.join(", ", line)+"}");
                }
                try {
                    Path file = Paths.get("xploration-results.txt");
                    Files.write(file, lines, Charset.forName("UTF-8"));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                System.out.println("Returning home.\n");
                System.out.println("\"Traveling through hyperspace ain't like dusting crops, farm boy.\"");
                System.out.println("- Han Solo\n");

                Ontology jmo = JADEManagementOntology.getInstance();
                getContentManager().registerOntology(jmo);
                ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
                msg.addReceiver(getAMS());
                msg.setLanguage(codec.getName());
                msg.setOntology(jmo.getName());
                try {
                    getContentManager().fillContent(msg, new Action(getAID(), new ShutdownPlatform()));
                    send(msg);
                }
                catch (Exception e) {
                    System.out.println("Failed to shutdown platform.");
                    e.printStackTrace();
                }
            }
        };
    }

}
