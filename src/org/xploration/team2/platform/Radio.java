package org.xploration.team2.platform;

import jade.content.Concept;
import jade.content.ContentElement;
import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.WakerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import org.xploration.ontology.ClaimCellInfo;
import org.xploration.ontology.MapBroadcastInfo;
import org.xploration.ontology.Team;
import org.xploration.ontology.XplorationOntology;
import org.xploration.team2.common.MessageBuilder;
import org.xploration.team2.platform.simulation.Simulation;
import org.xploration.team2.platform.simulation.SimulationAgent;

import java.util.Map;

/**
 * Created by Steven on 13/05/2017.
 */
public class Radio extends Agent {

    private Codec codec = new SLCodec();
    private Ontology ontology = XplorationOntology.getInstance();
    private Simulation simulation;

    protected void setup() {
        System.out.println(getLocalName() + ": has entered into the system");

        //Register of the codec and the ontology to be used in the ContentManager
        getContentManager().registerLanguage(codec);
        getContentManager().registerOntology(ontology);
        simulation = Simulation.getInstance();

        try {
            // Creates its own description
            DFAgentDescription dfd = new DFAgentDescription();
            ServiceDescription sdMapBroadcast = new ServiceDescription();
            sdMapBroadcast.setName(this.getName());
            sdMapBroadcast.setType(XplorationOntology.MAPBROADCASTSERVICE);
            ServiceDescription sdCellClaim = new ServiceDescription();
            sdCellClaim.setName(this.getName());
            sdCellClaim.setType(XplorationOntology.RADIOCLAIMSERVICE);
            dfd.addServices(sdCellClaim);
            // Registers its description in the DF
            DFService.register(this, dfd);
            System.out.println(getLocalName() + ": registered in the DF as MAP BROADCAST SERVICE");
        } catch (FIPAException e) {
            e.printStackTrace();
        }

        addBehaviour(getWaitAgentRegistrationBehaviour());
    }

    private Behaviour getWaitAgentRegistrationBehaviour() {
        return new SimpleBehaviour() {
            @Override
            public void action() {
                doWait(100);
            }

            @Override
            public boolean done() {
                if (simulation.allAgentsRegistered) {
                    addBehaviour(getMapBroadcastBehaviour());
                    addBehaviour(getCellClaimBehaviour());
                    addBehaviour(getMissionEndBehaviour());
                    return true;
                }
                return false;
            }
        };
    }

    private Behaviour getMapBroadcastBehaviour() {
        return new CyclicBehaviour(this) {
            @Override
            public void onStart() {
                System.out.println("Agents can now start broadcasting.");
            }

            @Override
            public void action() {
                // Waits for broadcasts
                ACLMessage msg = receive(
                    MessageTemplate.and(
                        MessageTemplate.MatchProtocol(XplorationOntology.MAPBROADCASTINFO),
                        MessageTemplate.and(
                                MessageTemplate.MatchLanguage(codec.getName()),
                                MessageTemplate.and(
                                        MessageTemplate.MatchOntology(ontology.getName()),
                                        MessageTemplate.MatchPerformative(ACLMessage.INFORM)
                                )
                        )
                    )
                );
                if (msg != null) {

                    ContentElement ce;
                    try {
                        ce = getContentManager().extractContent(msg);

                        if (ce instanceof Action) {
                            Action agAction = (Action) ce;
                            Concept conc = agAction.getAction();

                            if (conc instanceof MapBroadcastInfo) {
                                AID fromAgentAID = msg.getSender();
                                SimulationAgent fromAgent = simulation.getRover(fromAgentAID);
                                MapBroadcastInfo mbi = (MapBroadcastInfo) conc;

                                System.out.println(myAgent.getLocalName()+": received broadcast from "+(msg.getSender()).getLocalName()+" at "+fromAgent.getLocation().toString());
                                doForwardBroadcast(myAgent, fromAgent, mbi);
                            } else {
                                ACLMessage reply = MessageBuilder.getResponseMessage(msg, ACLMessage.NOT_UNDERSTOOD);
                                myAgent.send(reply);
                                System.out.println(myAgent.getLocalName()+": replied with NOT UNDERSTOOD.\tMessage: "+msg.getContent());
                            }
                        }
                    } catch (Codec.CodecException | OntologyException e) {
                        e.printStackTrace();

                        ACLMessage reply = MessageBuilder.getResponseMessage(msg, ACLMessage.NOT_UNDERSTOOD);
                        myAgent.send(reply);
                        System.out.println(myAgent.getLocalName()+": NOT UNDERSTOOD sent");
                    }
                }
            }

        };
    }

    private void doForwardBroadcast(Agent myAgent, SimulationAgent sender, MapBroadcastInfo mbi) {
        Map<AID, SimulationAgent> neighbours = simulation.getAgentsInSignalRange(sender.getLocation());
        neighbours.remove(sender.getAgentId());

        System.out.print(myAgent.getLocalName()+": forwarding broadcast from "+sender.getAgentId().getLocalName()+" at "+sender.getLocation().toString()+" to ");
        for(SimulationAgent neighbour : neighbours.values()) {
            System.out.print(neighbour.getAgentId().getLocalName()+" at "+neighbour.getLocation().toString()+", ");
            doForwardSingleBroadcast(neighbour.getAgentId(), mbi);
        }
        System.out.print("\n");
    }

    private void doForwardSingleBroadcast(AID recipientAID, MapBroadcastInfo mbi) {
        try {
            ACLMessage msg = MessageBuilder.getActionMessage(recipientAID, ACLMessage.INFORM, mbi, XplorationOntology.MAPBROADCASTINFO);
            send(msg);
        } catch (Codec.CodecException | OntologyException e) {
            e.printStackTrace();
        }

    }


    private Behaviour getCellClaimBehaviour() {
        return new CyclicBehaviour(this) {

            @Override
            public void action() {
                // Waits for broadcasts
                ACLMessage msg = receive(
                    MessageTemplate.and(
                        MessageTemplate.MatchProtocol(XplorationOntology.CLAIMCELLINFO),
                        MessageTemplate.and(
                            MessageTemplate.MatchLanguage(codec.getName()),
                            MessageTemplate.and(
                                MessageTemplate.MatchOntology(ontology.getName()),
                                MessageTemplate.MatchPerformative(ACLMessage.INFORM)
                            )
                        )
                    )
                );
                if (msg != null) {

                    ContentElement ce;
                    try {
                        ce = getContentManager().extractContent(msg);

                        if (ce instanceof Action) {
                            Action agAction = (Action) ce;
                            Concept conc = agAction.getAction();

                            if (conc instanceof ClaimCellInfo) {

                                AID fromAgentAID = msg.getSender();
                                SimulationAgent fromAgent = simulation.getRover(fromAgentAID);

                                ClaimCellInfo claimInfo = (ClaimCellInfo) conc;
                                Team targetTeam = claimInfo.getTeam();
                                SimulationAgent toAgent = simulation.getCapsule(targetTeam);

                                System.out.println(myAgent.getLocalName()+": received cell claim from "+(msg.getSender()).getLocalName()+" at "+fromAgent.getLocation().toString());

                                if (fromAgent.getTeam().getTeamId() == targetTeam.getTeamId() && simulation.areWithinRange(fromAgent, toAgent)) {
                                    System.out.println(myAgent.getLocalName()+": Valid TeamID & Capsule in Range - OK");

                                    ACLMessage forwardMsg = MessageBuilder.getActionMessage(toAgent.getAgentId(),
                                                                                     ACLMessage.INFORM,
                                                                                     claimInfo,
                                                                                     XplorationOntology.CLAIMCELLINFO);
                                    send(forwardMsg);
                                }
                            }
                        }
                    } catch (Codec.CodecException | OntologyException e) {
                        e.printStackTrace();
                        System.out.println(myAgent.getLocalName()+": INVALID CELL CLAIM INFORM!");
                    }
                }
            }

        };
    }

    private Behaviour getMissionEndBehaviour() {
        return new WakerBehaviour(this, Constants.MISSION_LENGTH) {
            @Override
            public void onWake() {
                this.myAgent.doDelete();
            }
        };
    }
}
