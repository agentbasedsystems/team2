package org.xploration.team2.platform;

import jade.content.Concept;
import jade.content.ContentElement;
import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.WakerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import org.xploration.ontology.Cell;
import org.xploration.ontology.CellAnalysis;
import org.xploration.ontology.XplorationOntology;
import org.xploration.team2.common.MapCell;
import org.xploration.team2.common.MessageBuilder;
import org.xploration.team2.platform.simulation.Rover;
import org.xploration.team2.platform.simulation.Simulation;

import java.awt.*;

/**
 * Created by Ivan on 27/04/2017.
 */
public class TerrainSimulator extends Agent {
    private Codec codec = new SLCodec();
    private Ontology ontology = XplorationOntology.getInstance();
    private Simulation simulation;

    protected void setup() {
        System.out.println(getLocalName()+": has entered into the system");
        //Register of the codec and the ontology to be used in the ContentManager
        getContentManager().registerLanguage(codec);
        getContentManager().registerOntology(ontology);

        simulation = Simulation.getInstance();

        try {
            // Creates its own description
            DFAgentDescription dfd = new DFAgentDescription();
            ServiceDescription sd = new ServiceDescription();
            sd.setName(this.getName());
            sd.setType(XplorationOntology.TERRAINSIMULATOR);
            dfd.addServices(sd);
            // Registers its description in the DF
            DFService.register(this, dfd);
            System.out.println(getLocalName()+": registered in the DF");
        } catch (FIPAException e) {
            e.printStackTrace();
        }

        addBehaviour(getWaitAgentRegistrationBehaviour());
    }

    private Behaviour getWaitAgentRegistrationBehaviour() {
        return new SimpleBehaviour() {
            @Override
            public void action() {
                doWait(100);
            }

            @Override
            public boolean done() {
                if (simulation.allAgentsRegistered) {
                    addBehaviour(getCellAnalysisListenerBehaviour());
                    addBehaviour(getMissionEndBehaviour());
                    return true;
                }
                return false;
            }
        };
    }

    private Behaviour getCellAnalysisListenerBehaviour() {
        return new CyclicBehaviour(this) {

            @Override
            public void action() {
                // Waits for MapCell Analysis requests
                ACLMessage msg = receive(
                        MessageTemplate.and(
                                MessageTemplate.MatchLanguage(codec.getName()),
                                MessageTemplate.and(
                                        MessageTemplate.MatchOntology(ontology.getName()),
                                        MessageTemplate.MatchPerformative(ACLMessage.REQUEST)
                                )
                        )
                );
                if (msg != null) {
                    // If an CELLANALYSIS request arrives (type REQUEST)
                    // it answers with the REFUSE, AGREE or NU

                    ContentElement ce;
                    try {
                        ce = getContentManager().extractContent(msg);

                        // We expect an action inside the message
                        if (ce instanceof Action) {
                            Action agAction = (Action) ce;
                            Concept conc = agAction.getAction();

                            // If the action is CellAnalysis...
                            if (conc instanceof CellAnalysis) {
                                AID fromAgent = msg.getSender();
                                System.out.println(myAgent.getLocalName()+": received MapCell Analysis request from "+(msg.getSender()).getLocalName());
                                Cell requestedCell = ((CellAnalysis) conc).getCell();
                                System.out.println(myAgent.getLocalName()+": MapCell Analysis request for ("+requestedCell.getX()+", "+requestedCell.getY()+")");

                                //If valid cell location - agree to analyse
                                if (simulation.isValidLocation(new Point(requestedCell.getX(), requestedCell.getY()))) {

                                    ACLMessage reply = MessageBuilder.getResponseMessage(msg, ACLMessage.AGREE);
                                    myAgent.send(reply);
                                    System.out.println(myAgent.getLocalName()+": AGREE response sent");

                                    //Add WakerBehaviour to fire after constant interval
                                    addBehaviour(getCellAnalysisResultBehaviour(fromAgent, requestedCell));
                                } else {

                                    ACLMessage reply = MessageBuilder.getResponseMessage(msg, ACLMessage.REFUSE);
                                    myAgent.send(reply);
                                    System.out.println(myAgent.getLocalName()+": REFUSE response sent");
                                }

                            } else {
                                ACLMessage reply = MessageBuilder.getResponseMessage(msg, ACLMessage.NOT_UNDERSTOOD);
                                myAgent.send(reply);
                                System.out.println(myAgent.getLocalName()+": replied with NOT UNDERSTOOD.\tMessage: "+msg.getContent());
                            }
                        }
                    } catch (Codec.CodecException | OntologyException e) {
                        e.printStackTrace();

                        ACLMessage reply = MessageBuilder.getResponseMessage(msg, ACLMessage.NOT_UNDERSTOOD);
                        myAgent.send(reply);
                        System.out.println(myAgent.getLocalName()+": NOT UNDERSTOOD sent");
                    }
                }
            }

        };
    }

    private Behaviour getCellAnalysisResultBehaviour(AID agentId, Cell requestedCell) {
        return new WakerBehaviour(this, Constants.CELL_ANALYSIS_TIME_MS) {

            protected void onWake() {
                Rover rover = Simulation.getInstance().getRover(agentId);
                Point requestedCellLocation = new Point(requestedCell.getX(), requestedCell.getY());
                MapCell requestedMapCell = simulation.getMap().getCell(requestedCellLocation);

                System.out.println(getLocalName() + ": Preparing CellAnalysis response for");
                System.out.println(getLocalName() + ": team: " + rover.getTeam().getTeamId());
                System.out.println(getLocalName() + ": rover location: " + rover.getLocation());
                System.out.println(getLocalName() + ": requested cell: " + requestedMapCell.getLocation());

                if (rover.getLocation().equals(requestedMapCell.getLocation())) {
                    //Valid request - provide response

                    Cell responseCell = requestedMapCell.getOntologyCell();
                    CellAnalysis cellAnalysis = new CellAnalysis();
                    cellAnalysis.setCell(responseCell);

                    try {
                        ACLMessage msg = MessageBuilder.getActionMessage(agentId,
                                                                         ACLMessage.INFORM,
                                                                         cellAnalysis,
                                                                         XplorationOntology.CELLANALYSIS);
                        send(msg);
                        System.out.println(getLocalName() + ": CellAnalysis INFORM sent");
                    } catch (Codec.CodecException | OntologyException e) {
                        e.printStackTrace();
                    }
                } else {
                    //Rover position does not match requested cell - return FAILURE

                    ACLMessage msg = MessageBuilder.getMessage(agentId, ACLMessage.FAILURE, XplorationOntology.CELLANALYSIS);
                    send(msg);
                    System.out.println(getLocalName() + ": CellAnalysis FAILURE sent");
                }
            }
        };
    }

    protected void takeDown() {
        // Deregister from the yellow pages
        try {
            DFService.deregister(this);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }

        // Printout a dismissal message
        System.out.println("TerrainSimulator agent "+getAID().getName()+" terminating.");
    }

    private Behaviour getMissionEndBehaviour() {
        return new WakerBehaviour(this, Constants.MISSION_LENGTH) {
            @Override
            public void onWake() {
                this.myAgent.doDelete();
            }
        };
    }
}
