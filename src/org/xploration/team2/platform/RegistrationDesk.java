package org.xploration.team2.platform;

import jade.content.Concept;
import jade.content.ContentElement;
import jade.content.lang.Codec;
import jade.content.lang.Codec.CodecException;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.ThreadedBehaviourFactory;
import jade.core.behaviours.WakerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import jade.wrapper.StaleProxyException;
import org.xploration.ontology.RegistrationRequest;
import org.xploration.ontology.Team;
import org.xploration.ontology.XplorationOntology;
import org.xploration.team2.common.MessageBuilder;

import java.util.LinkedList;
import java.util.List;

public class RegistrationDesk extends Agent {
    
    private Codec codec = new SLCodec();
    private Ontology ontology = XplorationOntology.getInstance();
    private ThreadedBehaviourFactory tbf;
    private List<Team> registrationList = new LinkedList<Team>();
    private boolean deskOpen = true;

    protected void setup() {
        System.out.println(getLocalName()+": has entered into the system");

        //Register of the codec and the ontology to be used in the ContentManager
        getContentManager().registerLanguage(codec);
        getContentManager().registerOntology(ontology);

        try {
            // Creates its own description
            DFAgentDescription dfd = new DFAgentDescription();
            ServiceDescription sd = new ServiceDescription();
            sd.setName(this.getName());
            sd.setType(XplorationOntology.REGISTRATIONDESK);
            dfd.addServices(sd);
            // Registers its description in the DF
            DFService.register(this, dfd);
            System.out.println(getLocalName()+": registered in the DF");
        } catch (FIPAException e) {
            e.printStackTrace();
        }

        tbf = new ThreadedBehaviourFactory();
        Behaviour registrationListener = getRegistrationListenerBehaviour();
        addBehaviour(tbf.wrap(registrationListener));

        addBehaviour(getLaunchBehaviour());
        addBehaviour(getMissionEndBehaviour());
    }

    private Behaviour getLaunchBehaviour() {
        return new WakerBehaviour(this, Constants.REGISTRATION_DESK_OPEN_TIME_MS) {
            protected void onWake() {
                deskOpen = false;
                System.out.println("Desk closed");
                System.out.println("REgistrations received: " + registrationList.size());
                if (registrationList.size() > 0) {
                    try {
                        ContainerController cc = getContainerController();
                        Object params[] = new Object[1];
                        params[0] = registrationList.toArray();
                        AgentController ac = cc.createNewAgent("SpaceCraft",
                                                               "org.xploration.team2.platform.SpaceCraft",
                                                                params);
                        ac.start();
                    } catch (StaleProxyException e) {
                        e.printStackTrace();
                    }
                } else {
                    System.out.println("No companies have registered :(");
                }
            }
        };
    }

    private Behaviour getRegistrationListenerBehaviour() {
        return new CyclicBehaviour(this) {

            @Override
            public void action() {
                // Waits for estimation requests
                ACLMessage msg = blockingReceive(
                        MessageTemplate.and(
                                MessageTemplate.MatchLanguage(codec.getName()),
                                MessageTemplate.and(
                                        MessageTemplate.MatchOntology(ontology.getName()),
                                        MessageTemplate.MatchPerformative(ACLMessage.REQUEST)
                                )
                        )
                );
                if (msg != null) {
                    // If an REGISTRATION request arrives (type REQUEST)
                    // it answers with the REFUSE, AGREE or NU

                    // The ContentManager transforms the message content (string)
                    // in java simulation
                    ContentElement ce;
                    try {
                        ce = getContentManager().extractContent(msg);

                        // We expect an action inside the message
                        if (ce instanceof Action) {
                            Action agAction = (Action) ce;
                            Concept conc = agAction.getAction();

                            // If the action is RegistrationRequest...
                            if (conc instanceof RegistrationRequest) {

                                AID fromAgent = msg.getSender();
                                System.out.println(myAgent.getLocalName()+": received registration request from "+(msg.getSender()).getLocalName());
                                Team requestorTeam = ((RegistrationRequest) conc).getTeam();
                                System.out.println(myAgent.getLocalName()+": registration request for "+requestorTeam);

                                if (deskOpen) {
                                    ACLMessage reply = MessageBuilder.getResponseMessage(msg, ACLMessage.AGREE);
                                    myAgent.send(reply);
                                    System.out.println(myAgent.getLocalName()+": RegistrationRequest AGREE sent");

                                    if (!registrationList.contains(requestorTeam)) {
                                        registrationList.add(requestorTeam);
                                        // Confirm registration
                                        ACLMessage finalMsg = MessageBuilder.getMessage(fromAgent, ACLMessage.INFORM, XplorationOntology.REGISTRATIONREQUEST);
                                        myAgent.send(finalMsg);
                                        System.out.println(myAgent.getLocalName()+": RegistrationRequest INFORM sent");
                                    } else {
                                        // Already registered registration
                                        ACLMessage finalMsg = MessageBuilder.getMessage(fromAgent, ACLMessage.FAILURE, XplorationOntology.REGISTRATIONREQUEST);
                                        myAgent.send(finalMsg);
                                        System.out.println(myAgent.getLocalName()+": RegistrationRequest FAILURE sent");
                                    }

                                } else {
                                    ACLMessage reply = MessageBuilder.getResponseMessage(msg, ACLMessage.REFUSE);
                                    myAgent.send(reply);
                                    System.out.println(myAgent.getLocalName()+": RegistrationRequest REFUSE sent");
                                }

                            } else {
                                ACLMessage reply = MessageBuilder.getResponseMessage(msg, ACLMessage.NOT_UNDERSTOOD);
                                myAgent.send(reply);
                                System.out.println(myAgent.getLocalName()+": replied with NOT UNDERSTOOD.\tMessage: "+msg.getContent());
                            }
                        }
                    } catch (CodecException | OntologyException e) {
                        e.printStackTrace();

                        ACLMessage reply = MessageBuilder.getResponseMessage(msg, ACLMessage.NOT_UNDERSTOOD);
                        myAgent.send(reply);
                        System.out.println(myAgent.getLocalName()+": NOT UNDERSTOOD sent");
                    }
                }
            }

        };
    }

    protected void takeDown() {
        // Deregister from the yellow pages
        try {
            DFService.deregister(this);
        }
        catch (FIPAException fe) {
            fe.printStackTrace();
        }
        
        // Printout a dismissal message
        System.out.println("RegistrationDesk agent "+getAID().getName()+" terminating.");
    }


    private Behaviour getMissionEndBehaviour() {
        return new WakerBehaviour(this, Constants.MISSION_LENGTH) {
            @Override
            public void onWake() {
                this.myAgent.doDelete();
            }
        };
    }


}
