package org.xploration.team2.platform;

public class Constants {
    public static final String FILENAME = "xploration.properties";
    public static final int REGISTRATION_DESK_OPEN_TIME_MS = Config.getInstance().getPropertyAsInt("REGISTRATION_WINDOW") * 1000;
    public static final int CELL_ANALYSIS_TIME_MS = Config.getInstance().getPropertyAsInt("ANALYSIS_TIME") * 1000;
    public static final int SIGNAL_RANGE = Config.getInstance().getPropertyAsInt("RADIO_RANGE");
    public static final int MISSION_LENGTH = Config.getInstance().getPropertyAsInt("MISSION_LENGTH") * 1000;
    public static final int MOVEMENT_TIME_MS = Config.getInstance().getPropertyAsInt("MOVEMENT_TIME") * 1000;
}
