package org.xploration.team2.company;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import org.xploration.ontology.RegistrationRequest;
import org.xploration.ontology.Team;
import org.xploration.ontology.XplorationOntology;
import org.xploration.team2.common.MessageBuilder;

public class Company extends Agent {
    private final int teamId = 2;
    
    protected void setup() {
        System.out.println(getLocalName()+": has entered into the system");

        addBehaviour(registerCompany());
    }
    
    private Behaviour registerCompany() {
        return new SimpleBehaviour(this) {
            
            private boolean requestedRegistration = false;
            
            @Override
            public void action() {
                
                // Creates the description for the type of agent to be searched
                DFAgentDescription dfd = new DFAgentDescription();
                ServiceDescription sd = new ServiceDescription();
                sd.setType(XplorationOntology.REGISTRATIONDESK);
                dfd.addServices(sd);
                
                try {
                    // It finds agents of the required type
                    DFAgentDescription[] res = new DFAgentDescription[20];
                    res = DFService.search(myAgent, dfd);
                    
                    // Gets the first occurrence, if there was success
                    if (res.length > 0) {
                        AID registrationDeskAgent = res[0].getName();
                        
                        // Asks the estimation to the painter
                        RegistrationRequest regRequest = new RegistrationRequest();
                        Team ownTeam = new Team();
                        ownTeam.setTeamId(teamId);
                        regRequest.setTeam(ownTeam);
                        ACLMessage msg = MessageBuilder.getActionMessage(registrationDeskAgent,
                                                                         ACLMessage.REQUEST,
                                                                         regRequest,
                                                                         XplorationOntology.REGISTRATIONREQUEST);
                        send(msg);
                        System.out.println(getLocalName()+": REQUESTS A REGISTRATION");

                        requestedRegistration = true;
                    } else {
                        // If no RegistrationDesk has been found, wait 5 seconds
                        doWait(5000);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public boolean done() {
                return requestedRegistration;
            }
        };
    }
}
