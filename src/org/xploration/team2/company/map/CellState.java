package org.xploration.team2.company.map;

/**
 * Created by Ivan on 13/05/2017.
 */
public enum CellState {
    UNKNOWN,
    ANALYZED,
    CLAIMED,
    SHARED //Cell obtained from other agents
}
