package org.xploration.team2.company.map;

import jade.util.leap.ArrayList;
import jade.util.leap.List;
import org.xploration.ontology.Cell;
import org.xploration.team2.common.AsteroidMap;
import org.xploration.team2.common.MapCell;

import java.awt.*;

public class AgentMap extends AsteroidMap {

	public AgentMap(int n, int m) {
		super();
		this.n = n; //Height
		this.m = m; //Width

		initMap();
	}

	private void initMap() {
		Point location;
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= m; j++) {
				if ((i % 2) == (j % 2)) {
					location = new Point(i, j);
					map.put(location, new AgentMapCell(location, CellState.UNKNOWN));
				}
			}
		}
	}

	public AgentMapCell getCell(Point location) {
		return (AgentMapCell) map.get(location);
	}

	public void addAnalysedCell(Cell cell) {
		AgentMapCell mapCell = (AgentMapCell) map.get(new Point(cell.getX(), cell.getY()));
		mapCell.setMineral(cell.getMineral());
		mapCell.setState(CellState.ANALYZED);
	}

	public void addClaimedCell(Cell cell) {
		AgentMapCell mapCell = (AgentMapCell) map.get(new Point(cell.getX(), cell.getY()));
		mapCell.setState(CellState.CLAIMED);
	}

	public void addSharedCell(Cell cell) {
		AgentMapCell mapCell = (AgentMapCell) map.get(new Point(cell.getX(), cell.getY()));
		if (mapCell.getMineral() == null) {
			mapCell.setMineral(cell.getMineral());
		}
		//Only setting state to shared if we first learned about this cell from others
		if (mapCell.getState() == CellState.UNKNOWN) {
			mapCell.setState(CellState.SHARED);
		}
	}

	public void addSharedCells(List cells) {
		for(int i = 0; i < cells.size(); i++) {
			Cell cell = (Cell) cells.get(i);
			addSharedCell(cell);
		}
	}

	public void addClaimedCells(List cells) {
		for(int i = 0; i < cells.size(); i++) {
			Cell cell = (Cell) cells.get(i);
			addClaimedCell(cell);
		}
	}

	public List getKnownCells() {
		List knownCells = new ArrayList();

		for (MapCell mapCell : map.values()) {
			AgentMapCell agentMapCell = (AgentMapCell)mapCell;
			if (agentMapCell.getState() == CellState.CLAIMED || agentMapCell.getState() == CellState.SHARED) {
				knownCells.add(agentMapCell.getOntologyCell());
			} else if(agentMapCell.getState() == CellState.ANALYZED) {
				knownCells.add(agentMapCell.getFaultyOntologyCell());
			}
		}

		return knownCells;
	}

	public java.util.ArrayList getKnownCellsAsPoints() {
		java.util.ArrayList knownCells = new java.util.ArrayList();
		for (MapCell mapCell : map.values()) {
			AgentMapCell agentMapCell = (AgentMapCell)mapCell;
			//I as
			if (agentMapCell.getState() == CellState.SHARED) {
				knownCells.add(agentMapCell.getLocation());
			}
		}

		return knownCells;
	}

	public List getClaimableCells() {
		List claimableCells = new ArrayList();

		for (MapCell mapCell : map.values()) {
			AgentMapCell agentMapCell = (AgentMapCell)mapCell;
			if (agentMapCell.getState() == CellState.ANALYZED) {
				claimableCells.add(agentMapCell.getOntologyCell());
			}
		}

		return claimableCells;
	}
}
