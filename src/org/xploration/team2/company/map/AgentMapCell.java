package org.xploration.team2.company.map;

import org.xploration.ontology.Cell;
import org.xploration.team2.common.MapCell;

import java.awt.*;

/**
 * Created by Ivan on 13/05/2017.
 */
public class AgentMapCell extends MapCell {

    private CellState state;

    public AgentMapCell(Point location, CellState state) {
        super(location);
        this.state = state;
    }

    public AgentMapCell(Point location, String mineral, CellState state) {
        this(location, state);
        this.mineral = mineral;
    }

    public AgentMapCell(Cell cell, CellState state) {
        this(new Point(cell.getX(), cell.getY()), cell.getMineral(), state);
    }

    public CellState getState() {
        return state;
    }

    public void setState(CellState state) {
        this.state = state;
    }
}
