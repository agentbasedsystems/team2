package org.xploration.team2.company;

import jade.content.Concept;
import jade.content.ContentElement;
import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.*;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import org.xploration.ontology.*;
import org.xploration.team2.common.Direction;
import org.xploration.team2.common.MessageBuilder;
import org.xploration.team2.common.RoverState;
import org.xploration.team2.company.map.AgentMap;
import org.xploration.team2.company.map.CellState;

import java.awt.*;
import java.util.ArrayList;
import java.util.Date;


public class Rover2 extends Agent {

    private final Team team = new Team();
    private AgentMap map;
    private int missionLength;
    private int radioRange;

    private XplorationOntology ontology = (XplorationOntology) XplorationOntology.getInstance();
    private Codec codec = new SLCodec();
    private Point location = new Point();
    private Direction direction;

    private Point capsuleLocation;

    private ThreadedBehaviourFactory tbf;

    private RoverState state;

    /*movement stuff*/
    private int analysisTime = 0;
    private int movementTime = 0;
    private long counterTime;
    int radius = 1;
    private Point originalLocation;
    private ArrayList<ArrayList> edgeList = new ArrayList<>();
    private ArrayList pointsInRange;
    private Boolean isNotUsingEdge = false;
    private Boolean noMoreMovements = false;
    private int remainingCells = 0;

    protected void setup() {
        System.out.println(getLocalName() + " has entered into the system");
        getContentManager().registerOntology(ontology);
        getContentManager().registerLanguage(codec);

        team.setTeamId(2);
        state = RoverState.IDLE;

        Object[] args = getArguments();
        location = new Point((int) args[0], (int) args[1]);
        originalLocation = location;
        capsuleLocation = new Point(location);
        int height = (int) args[2];
        int width = (int) args[3];
        map = new AgentMap(height, width);
        System.out.println(getLocalName() + " Starting location: " + location.toString());
        System.out.println(getLocalName() + " World dimensions: (" + height + "; " + width + ")");
        missionLength = ((int) args[4])*1000;
        radioRange = (int) args[5];

        tbf = new ThreadedBehaviourFactory();

        addBehaviour(getRoverRegistrationBehaviour());
        addBehaviour(getMissionEndBehaviour());
    }

    private void setState(RoverState state) {
        System.out.println(getLocalName()+": STATE = " + state);
        this.state = state;
    }

    private void decideWhatToDo() {
        //Regardless of previous action, if capsule is within range
        //and there are cells to be claimed - claim
        if ((map.distance(location, capsuleLocation) <= radioRange) && map.getClaimableCells().size() > 0) {
            addBehaviour(getClaimBehaviour());
        }

        if (state == RoverState.ANALYZING) {
            //If analyzed, then move
            direction = nextDirection();
            if (direction == null){
            	System.out.println("NULL received");
            	direction = Direction.SOUTH;
            }
            addBehaviour(getMovementRequestBehaviour());
        } else {
            //Arrived to new cell or was IDLE
            if (map.getCell(location).getState() == CellState.UNKNOWN) {
                //analyse
                addBehaviour(getCellAnalysisBehaviour());
            } else {
                //move again
                direction = nextDirection();
                if (direction == null){
                	System.out.println("NULL received");
                    direction = Direction.SOUTH;
                }
                addBehaviour(getMovementRequestBehaviour());
            }
        }
    }

    private Direction nextDirection(){
        if (movementTime == 0){
            System.out.println("Winter is comming!!!");
            isNotUsingEdge = true;
            return Direction.SOUTH; //Winter is coming
        }else{
            Boolean useEdge = false;
            int indexArray = 0;

            //Purge edge array list
            for (int i = 0; i < edgeList.size();i++){
                if (edgeList.get(i).size()==0){
                    edgeList.remove(i);
                    if (edgeList.size()!=1) {
                        isNotUsingEdge = false;
                    }
                }
            }

            switch (edgeList.size()){
                case 0:
                    //Finish the last cells
                    break;
                case 1:
                    if (isNotUsingEdge && noMoreMovements){
                    	indexArray = 0;
                    	useEdge = true;
                    }
                    break;
                case 2:
                    if (!isNotUsingEdge){
                        break;
                    }else{
                        int minSize = 999999;
                        for (int i = 0; i < 2; i++){
                            if (edgeList.get(i).size()<minSize){
                                minSize = edgeList.get(i).size();
                                indexArray = i;
                            }
                        }
                        useEdge = true;
                        break;
                    }
                case 3:
                    if (!isNotUsingEdge){
                        break;
                    }else{
                        int minSize = 999999;
                        for (int i = 0; i < 3; i++){
                            if (edgeList.get(i).size()<minSize){
                                minSize = edgeList.get(i).size();
                                indexArray = i;
                            }
                        }
                        useEdge = true;
                        break;
                    }
                case 4:
                    if (!isNotUsingEdge){
                        break;
                    }else{
                        int minSize = 999999;
                        for (int i = 0; i < 4; i++){
                            if (edgeList.get(i).size()<minSize){
                                minSize = edgeList.get(i).size();
                                indexArray = i;
                            }
                        }
                        useEdge = true;
                        break;
                    }
                case 5:
                    if (!isNotUsingEdge){
                        break;
                    }else{
                        int minSize = 999999;
                        for (int i = 0; i < 5; i++){
                            if (edgeList.get(i).size()<minSize){
                                minSize = edgeList.get(i).size();
                                indexArray = i;
                            }
                        }
                        useEdge = true;
                        break;
                    }
                case 6:
                    if (!isNotUsingEdge){
                        break;
                    }else{
                        int minSize = 999999;
                        for (int i = 0; i < 6; i++){
                            if (edgeList.get(i).size()<minSize){
                                minSize = edgeList.get(i).size();
                                indexArray = i;
                            }
                        }
                        useEdge = true;
                        break;
                    }

            }

            if (useEdge){
                useEdge = false;
                Point nextCell = (Point) edgeList.get(indexArray).get(0);
                edgeList.get(indexArray).remove(0);
                return getDirection(location,nextCell);
            }else {
                //Get Possible Movements
                ArrayList<Point> possibleMovements = new ArrayList<>();
                for (Object point : pointsInRange) {
                    if (map.isValidMovement(location, (Point) point)) {
                        possibleMovements.add((Point) point);
                    }
                }
                //Get further movements to the origin
                if (edgeList.size() == 1 && possibleMovements.size() == 0){
                    noMoreMovements = true;
                }
                ArrayList<Point> bestMovements = new ArrayList<>();
                int maxLength = 0;
                for (Object point : possibleMovements) {
                    if (map.distance(originalLocation, (Point) point) > maxLength) {
                        maxLength = map.distance(originalLocation, (Point) point);
                        bestMovements.add(0, (Point) point);
                    }
                }
                if (!bestMovements.isEmpty()) {
                    pointsInRange.remove(pointsInRange.indexOf(bestMovements.get(0)));
                    return getDirection(location, bestMovements.get(0));
                } else {
                    //No movement? Go to another edge
                    for (int i = 0; i < edgeList.size(); i++) {
                        if (map.isValidMovement(location, (Point) edgeList.get(i).get(0))) {
                            Point point = (Point) edgeList.get(i).get(0);
                            edgeList.get(i).remove(0);
                            isNotUsingEdge = true;
                            if (edgeList.size() == 1 && possibleMovements.size() == 0){
                                noMoreMovements = true;
                            }
                            return getDirection(location, point);
                        }
                    }
                }
            }
            if (pointsInRange.isEmpty()){
                return Direction.SOUTH_WEST;
            }
        }
        return null;
    }

    private Direction getDirection(Point location, Point nextCell) {
        if (map.coordinateInDirection(location,Direction.NORTH).equals(nextCell)){
            return Direction.NORTH;
        }
        if (map.coordinateInDirection(location,Direction.NORTH_WEST).equals(nextCell)){
            return Direction.NORTH_WEST;
        }
        if (map.coordinateInDirection(location,Direction.NORTH_EAST).equals(nextCell)){
            return Direction.NORTH_EAST;
        }
        if (map.coordinateInDirection(location,Direction.SOUTH_WEST).equals(nextCell)){
            return Direction.SOUTH_WEST;
        }
        if (map.coordinateInDirection(location,Direction.SOUTH_EAST).equals(nextCell)){
            return Direction.SOUTH_EAST;
        }
        return Direction.SOUTH;
    }

    private void generateMovementMap() {
        int numberOfCellsAnalyzable = (missionLength / 1000) / (analysisTime + movementTime);
        int accumulated = 0;
        int overlayingMap = 0;
        ArrayList<Point> edgeControl = new ArrayList<>();

        ArrayList<Point> S_edge = new ArrayList<>();
        ArrayList<Point> SE_edge = new ArrayList<>();
        ArrayList<Point> SW_edge = new ArrayList<>();
        ArrayList<Point> N_edge = new ArrayList<>();
        ArrayList<Point> NE_edge = new ArrayList<>();
        ArrayList<Point> NW_edge = new ArrayList<>();

        for(;numberOfCellsAnalyzable > accumulated+radius*6;radius++){
            accumulated = accumulated + radius*6;
        }
        remainingCells = (accumulated+radius*6-numberOfCellsAnalyzable);

        //Get points inside the hexagon (circle)
        pointsInRange = map.getListOfPointsInRange(radius,originalLocation);
        Point tempPoint = originalLocation;
        //Original Point is already analyzed.
        pointsInRange.remove(pointsInRange.indexOf(tempPoint));

        //Generate edges, to determine the slices.
        tempPoint = originalLocation;
        for (int i = 1;i<=radius;i++){
            tempPoint = map.coordinateInDirection(tempPoint,Direction.SOUTH);
            if (!edgeControl.contains(tempPoint)) {
                S_edge.add(tempPoint);
                edgeControl.add(tempPoint);
                if (pointsInRange.indexOf(tempPoint) != -1) {
                    pointsInRange.remove(pointsInRange.indexOf(tempPoint));
                }
            }else{
                overlayingMap++;
            }
        }

        tempPoint = originalLocation;
        for (int i = 1;i<=radius;i++){
            tempPoint = map.coordinateInDirection(tempPoint,Direction.SOUTH_EAST);
            if (!edgeControl.contains(tempPoint)) {
                SE_edge.add(tempPoint);
                edgeControl.add(tempPoint);
                if (pointsInRange.indexOf(tempPoint) != -1) {
                    pointsInRange.remove(pointsInRange.indexOf(tempPoint));
                }
            }else{
                overlayingMap++;
            }
        }

        tempPoint = originalLocation;
        for (int i = 1;i<=radius;i++){
            tempPoint = map.coordinateInDirection(tempPoint,Direction.SOUTH_WEST);
            if (!edgeControl.contains(tempPoint)) {
                SW_edge.add(tempPoint);
                edgeControl.add(tempPoint);
                if (pointsInRange.indexOf(tempPoint) != -1) {
                    pointsInRange.remove(pointsInRange.indexOf(tempPoint));
                }
            }else{
                overlayingMap++;
            }
        }

        tempPoint = originalLocation;
        for (int i = 1;i<=radius;i++){
            tempPoint = map.coordinateInDirection(tempPoint,Direction.NORTH);
            if (!edgeControl.contains(tempPoint)) {
                N_edge.add(tempPoint);
                edgeControl.add(tempPoint);
                if (pointsInRange.indexOf(tempPoint) != -1) {
                    pointsInRange.remove(pointsInRange.indexOf(tempPoint));
                }
            }else{
                overlayingMap++;
            }
        }

        tempPoint = originalLocation;
        for (int i = 1;i<=radius;i++){
            tempPoint = map.coordinateInDirection(tempPoint,Direction.NORTH_EAST);
            if (!edgeControl.contains(tempPoint)) {
                NE_edge.add(tempPoint);
                edgeControl.add(tempPoint);
                if (pointsInRange.indexOf(tempPoint) != -1) {
                    pointsInRange.remove(pointsInRange.indexOf(tempPoint));
                }
            }else{
                overlayingMap++;
            }
        }

        tempPoint = originalLocation;
        for (int i = 1;i<=radius;i++){
            tempPoint = map.coordinateInDirection(tempPoint,Direction.NORTH_WEST);
            if (!edgeControl.contains(tempPoint)) {
                NW_edge.add(tempPoint);
                edgeControl.add(tempPoint);
                if (pointsInRange.indexOf(tempPoint) != -1) {
                    pointsInRange.remove(pointsInRange.indexOf(tempPoint));
                }
            }else{
                overlayingMap++;
            }
        }

        //Rover went south manally, skipping the first point because the rover is already there.
        S_edge.remove(0);
        if (!S_edge.isEmpty()) {
            edgeList.add(S_edge);
        }
        edgeList.add(SE_edge);
        edgeList.add(SW_edge);
        edgeList.add(N_edge);
        edgeList.add(NE_edge);
        edgeList.add(NW_edge);

        if (overlayingMap > 0){
            //Snail strategy
            for (ArrayList list : edgeList){
                for (Object point : list){
                    pointsInRange.add(point);
                }
            }
        }
    }

    private void updateTripPlan() {
        //Removing navigable cells that are already explored
        ArrayList knownCells = map.getKnownCellsAsPoints();
        for (Object cell : knownCells ){
            for (ArrayList list : edgeList){
                if (list.contains(cell)){
                    list.remove(list.indexOf(cell));
                    remainingCells--;
                }
            }
            if (pointsInRange.contains(cell)){
                pointsInRange.remove(pointsInRange.indexOf(cell));
                remainingCells--;
            }
        }
        //We need to get more cells to explore
        radius++;
        ArrayList extraPoints = map.getListOfPointsInTheExactlyRange(radius, originalLocation);
        for (Object point : extraPoints){
            pointsInRange.add(point);
        }
    }

    private Behaviour getRoverRegistrationBehaviour() {
        return new SimpleBehaviour(this) {

            private boolean registeredWithPlatform = false;

            @Override
            public void action() {
                // Creates the description for the type of agent to be searched
                DFAgentDescription dfd = new DFAgentDescription();
                ServiceDescription sd = new ServiceDescription();
                sd.setType(XplorationOntology.ROVERREGISTRATIONSERVICE);
                dfd.addServices(sd);

                try {
                    // It finds agents of the required type
                    DFAgentDescription[] res = new DFAgentDescription[20];
                    res = DFService.search(myAgent, dfd);

                    // Gets the first occurrence, if there was success
                    if (res.length > 0) {
                        AID roverRegistrationAgent = res[0].getName();

                        // Asks the estimation to the painter
                        RoverRegistrationInfo regInfo = new RoverRegistrationInfo();
                        regInfo.setTeam(team);
                        Cell currCell = new Cell();
                        currCell.setX(((Double)location.getX()).intValue());
                        currCell.setY(((Double)location.getY()).intValue());
                        regInfo.setCell(currCell);
                        ACLMessage msg = MessageBuilder.getActionMessage(roverRegistrationAgent,
                                                                         ACLMessage.INFORM,
                                                                         regInfo,
                                                                         XplorationOntology.ROVERREGISTRATIONINFO);
                        send(msg);
                        System.out.println(getLocalName()+":  REGISTERS with the platform");

                        registeredWithPlatform = true;

                        //Once registered - can start broadcasting map
                        addBehaviour(getBroadcastMapBehaviour());
                        addBehaviour(getListenMapBroadcastThreadedBehaviour());

                        //Once registered - start by analysing cell it landed on
                        addBehaviour(getCellAnalysisBehaviour());
                    } else {
                        // If no RegistrationDesk has been found, wait 5 seconds
                        doWait(1000);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public boolean done() {
                return registeredWithPlatform;
            }
        };
    }

    private Behaviour getCellAnalysisBehaviour() {
        return new SimpleBehaviour(this) {

            private boolean requestedCellAnalysis = false;

            @Override
            public void action() {

                // Creates the description for the type of agent to be searched
                DFAgentDescription dfd = new DFAgentDescription();
                ServiceDescription sd = new ServiceDescription();
                sd.setType(XplorationOntology.TERRAINSIMULATOR);
                dfd.addServices(sd);

                try {
                    // It finds agents of the required type
                    DFAgentDescription[] res = new DFAgentDescription[20];
                    res = DFService.search(myAgent, dfd);

                    // Gets the first occurrence, if there was success
                    if (res.length > 0) {
                        AID terrainSimAgent = res[0].getName();

                        // Asks the estimation to the painter
                        CellAnalysis cellAnalysis = new CellAnalysis();
                        Cell cell = new Cell();
                        cell.setX((int) location.getX());
                        cell.setY((int) location.getY());
                        cellAnalysis.setCell(cell);

                        ACLMessage msg = MessageBuilder.getActionMessage(terrainSimAgent,
                                                                         ACLMessage.REQUEST,
                                                                         cellAnalysis,
                                                                         XplorationOntology.CELLANALYSIS);
                        send(msg);
                        if (analysisTime == 0){
                            counterTime = new Date().getTime();
                        }
                        System.out.println(getLocalName()+": REQUESTS A CELL ANALYSIS");

                        requestedCellAnalysis = true;
                        setState(RoverState.ANALYZING);

                        addBehaviour(getListenCellAnalysisThreadedBehaviour());
                    } else {
                        // If no TerrainSimulator has been found, wait 5 seconds
                        doWait(5000);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public boolean done() {
                return requestedCellAnalysis;
            }
        };
    }

    private Behaviour getListenCellAnalysisThreadedBehaviour() {
        return tbf.wrap(new SimpleBehaviour() {

            private boolean finishedAnalysing = false;

            @Override
            public void action() {
                // Waits for Cell Analysis response
                ACLMessage msg = receive(
                    MessageTemplate.and(
                        MessageTemplate.MatchLanguage(codec.getName()),
                        MessageTemplate.and(
                            MessageTemplate.MatchOntology(ontology.getName()),
                            MessageTemplate.MatchProtocol(XplorationOntology.CELLANALYSIS)
                        )
                    )
                );

                if (msg != null) {
                    // If an CELLANALYSIS request arrives (type REQUEST)
                    // it answers with the REFUSE, AGREE or NU

                    System.out.println(myAgent.getLocalName()+": ########## RECEIVED CELL ANALYSYS RESPONSE ##########");

                    if (msg.getPerformative() == ACLMessage.REFUSE) {
                        System.out.println(myAgent.getLocalName()+": REFUSE - invalid cell coordinates");
                        finishedAnalysing = true;
                    } else if (msg.getPerformative() == ACLMessage.NOT_UNDERSTOOD) {
                        System.out.println(myAgent.getLocalName()+": NOT UNDERSTOOD");
                        finishedAnalysing = true;
                    } else if (msg.getPerformative() == ACLMessage.AGREE) {
                        System.out.println(myAgent.getLocalName()+": AGREE - will supply result later");
                    } else if (msg.getPerformative() == ACLMessage.FAILURE) {
                        System.out.println(myAgent.getLocalName()+": FAILURE - rover and cell coordinates don't match");
                        finishedAnalysing = true;
                    } else if (msg.getPerformative() == ACLMessage.INFORM) {
                        System.out.println(myAgent.getLocalName()+": INFORM - see results below");

                        if (analysisTime == 0){
                            long currentDate = new Date().getTime();
                            analysisTime = (int) ((currentDate - counterTime)/1000);
                        }

                        ContentElement ce;
                        try {
                            ce = getContentManager().extractContent(msg);

                            // We expect an action inside the message
                            if (ce instanceof Action) {
                                Action agAction = (Action) ce;
                                Concept conc = agAction.getAction();

                                // If the action is CellAnalysis...
                                if (conc instanceof CellAnalysis) {
                                    Cell cell = ((CellAnalysis) conc).getCell();
                                    map.addAnalysedCell(cell);
                                    System.out.println(myAgent.getLocalName()+": LOCATION: ("+cell.getX()+","+cell.getY()+")");
                                    System.out.println(myAgent.getLocalName()+": MINERAL: "+cell.getMineral());

                                } else {
                                    ACLMessage reply = MessageBuilder.getResponseMessage(msg, ACLMessage.NOT_UNDERSTOOD);
                                    myAgent.send(reply);
                                    System.out.println(myAgent.getLocalName()+": replied with NOT UNDERSTOOD.\tMessage: "+msg.getContent());
                                }
                            }
                        } catch (Codec.CodecException | OntologyException e) {
                            e.printStackTrace();
                        } finally {
                            finishedAnalysing = true;
                        }
                    }
                } else {
                    doWait(100);
                }
            }

            @Override
            public boolean done() {
                if (finishedAnalysing) {
                    decideWhatToDo();
                }
                return finishedAnalysing;
            }
        });
    }

    private Behaviour getMovementRequestBehaviour() {
        return new SimpleBehaviour(this) {
            private Boolean MovementRequestRequested = false;
            public void action() {

                DFAgentDescription dfd = new DFAgentDescription();
                ServiceDescription sd = new ServiceDescription();
                sd.setType(XplorationOntology.MOVEMENTREQUESTSERVICE);
                dfd.addServices(sd);
                try {
                    // It finds agents of the required type
                    DFAgentDescription[] res = new DFAgentDescription[20];
                    res = DFService.search(myAgent, dfd);

                    // Gets the first occurrence, if there was success
                    if (res.length > 0) {
                        AID movementAID = res[0].getName();

                        MovementRequestInfo movementRequestInfo = new MovementRequestInfo();
                        Point destination = map.coordinateInDirection(location, direction);
                        Cell cell = new Cell();
                        cell.setX((int) destination.getX());
                        cell.setY((int) destination.getY());
                        movementRequestInfo.setCell(cell);

                        ACLMessage msg = MessageBuilder.getActionMessage(movementAID,
                                ACLMessage.REQUEST,
                                movementRequestInfo,
                                XplorationOntology.MOVEMENTREQUESTINFO);
                        send(msg);
                        if (movementTime == 0){
                            counterTime = new Date().getTime();
                        }
                        System.out.println(getLocalName()+": >>MOVEMENT REQUESTED TO MOVEMENTSIMULATOR: destination = " + destination + " : current location = " + location);

                        MovementRequestRequested = true;
                        setState(RoverState.MOVING);
                        addBehaviour(getListenMovementRequestThreadedBehaviour());
                    } else {
                        // If no TerrainSimulator has been found, wait 5 seconds
                        doWait(1000);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public boolean done() {
                return MovementRequestRequested;
            }
        };
    }

    private Behaviour getListenMovementRequestThreadedBehaviour() {
        return tbf.wrap(new SimpleBehaviour(this) {

            private boolean finishedMoving = false;

            @Override
            public void action() {
                // Waits for Movement Request response
                ACLMessage msg = receive(
                    MessageTemplate.and(
                        MessageTemplate.MatchLanguage(codec.getName()),
                        MessageTemplate.and(
                            MessageTemplate.MatchOntology(ontology.getName()),
                            MessageTemplate.MatchProtocol(XplorationOntology.MOVEMENTREQUESTINFO)
                        )
                    )
                );

                if (msg != null) {
                    // If an MOVEMENTREQUEST inform arrives (type inform)
                    // it answers with the REFUSE, AGREE or NU

                    if (msg.getPerformative() == ACLMessage.REFUSE) {
                        System.out.println(myAgent.getLocalName()+": >>REFUSE - Movement in invalid cell coordinates");
                        finishedMoving = true;
                    } else if (msg.getPerformative() == ACLMessage.NOT_UNDERSTOOD) {
                        System.out.println(myAgent.getLocalName()+": >>Movement Request NOT UNDERSTOOD");
                        finishedMoving = true;
                    } else if (msg.getPerformative() == ACLMessage.AGREE) {
                        System.out.println(myAgent.getLocalName()+": >>AGREE - Movement will supply result later");
                    } else if (msg.getPerformative() == ACLMessage.FAILURE) {
                        System.out.println(myAgent.getLocalName()+": >>FAILURE - Illegal Movement");
                        finishedMoving = true;
                    } else if (msg.getPerformative() == ACLMessage.INFORM) {
                        System.out.println(myAgent.getLocalName()+": >>INFORM - The movement is valid");
                        finishedMoving = true;
                        if (movementTime == 0){
                            long currentDate = new Date().getTime();
                            movementTime = (int) ((currentDate - counterTime)/1000);
                            generateMovementMap();
                        }
                        //Update new location
                        location = map.coordinateInDirection(location, direction);
                    }
                } else {
                    doWait(100);
                }
            }

            @Override
            public boolean done() {
                if (finishedMoving) {
                    decideWhatToDo();
                }
                return finishedMoving;
            }

        });
    }

    private Behaviour getBroadcastMapBehaviour() {
        return new TickerBehaviour(this, 5000) {
            AID mapBroadcastServiceAID;

            @Override
            public void onStart() {
                // Creates the description for the type of agent to be searched
                DFAgentDescription dfd = new DFAgentDescription();
                ServiceDescription sd = new ServiceDescription();
                sd.setType(XplorationOntology.MAPBROADCASTSERVICE);
                dfd.addServices(sd);

                try {
                    // It finds agents of the required type
                    DFAgentDescription[] res = new DFAgentDescription[20];
                    res = DFService.search(myAgent, dfd);

                    // Gets the first occurrence, if there was success
                    if (res.length > 0) {
                        mapBroadcastServiceAID = res[0].getName();
                    }
                } catch (FIPAException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onTick() {
                if(mapBroadcastServiceAID != null && map.getKnownCells().size() > 0) {
                    try {
                        ACLMessage msg = MessageBuilder.getActionMessage(mapBroadcastServiceAID, ACLMessage.INFORM, getMapBroadcastInfo(), XplorationOntology.MAPBROADCASTINFO);
                        send(msg);
                    } catch (Codec.CodecException | OntologyException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
    }

    private Behaviour getListenMapBroadcastThreadedBehaviour() {
        return tbf.wrap(new CyclicBehaviour(this) {

            @Override
            public void action() {
                // Waits for Cell Analysis response
                ACLMessage msg = receive(
                        MessageTemplate.and(
                                MessageTemplate.MatchLanguage(codec.getName()),
                                MessageTemplate.and(
                                        MessageTemplate.MatchOntology(ontology.getName()),
                                        MessageTemplate.MatchProtocol(XplorationOntology.MAPBROADCASTINFO)
                                )
                        )
                );

                if (msg != null) {
                    // If an MAPBROADCASTINFO inform arrives (type REQUEST)
                    // it answers with the REFUSE, AGREE or NU

                    if (msg.getPerformative() == ACLMessage.NOT_UNDERSTOOD) {
                        System.out.println(myAgent.getLocalName()+": NOT UNDERSTOOD - previous brodcast by this rover was not understood");
                    } else if (msg.getPerformative() == ACLMessage.INFORM) {
                        System.out.println(myAgent.getLocalName()+": INFORM - got data:");

                        ContentElement ce;
                        try {
                            ce = getContentManager().extractContent(msg);

                            // We expect an action inside the message
                            if (ce instanceof Action) {
                                Action agAction = (Action) ce;
                                Concept conc = agAction.getAction();

                                // If the action is MapBroadcastInfo...
                                if (conc instanceof MapBroadcastInfo) {
                                    System.out.println(myAgent.getLocalName()+": received map from "+(msg.getSender()).getLocalName());

                                    MapBroadcastInfo mbi = (MapBroadcastInfo) conc;
                                    map.addSharedCells(mbi.getMap().getCellList());
                                    updateTripPlan();
                                } else {
                                    ACLMessage reply = MessageBuilder.getResponseMessage(msg, ACLMessage.NOT_UNDERSTOOD);
                                    myAgent.send(reply);
                                    System.out.println(myAgent.getLocalName()+": replied with NOT UNDERSTOOD.\tMessage: "+msg.getContent());
                                }
                            }
                        } catch (Codec.CodecException | OntologyException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    doWait(100);
                }
            }
        });
    }

    private MapBroadcastInfo getMapBroadcastInfo() {
        MapBroadcastInfo mpi = new MapBroadcastInfo();
        Map knownMap = new Map();
        knownMap.setCellList(map.getKnownCells());
        mpi.setMap(knownMap);
        return mpi;
    }

    private Behaviour getClaimBehaviour() {
        return new SimpleBehaviour(this) {

            private boolean claimedCells = false;

            @Override
            public void action() {
                // Creates the description for the type of agent to be searched
                DFAgentDescription dfd = new DFAgentDescription();
                ServiceDescription sd = new ServiceDescription();
                sd.setType(XplorationOntology.RADIOCLAIMSERVICE);
                dfd.addServices(sd);

                try {
                    // It finds agents of the required type
                    DFAgentDescription[] res = new DFAgentDescription[20];
                    res = DFService.search(myAgent, dfd);

                    // Gets the first occurrence, if there was success
                    if (res.length > 0) {
                        AID radioClaimAgent = res[0].getName();

                        ClaimCellInfo claimInfo = new ClaimCellInfo();
                        Map claimMap = new Map();
                        claimMap.setCellList(map.getClaimableCells());
                        claimInfo.setMap(claimMap);
                        claimInfo.setTeam(team);

                        ACLMessage msg = MessageBuilder.getActionMessage(radioClaimAgent,
                                ACLMessage.INFORM,
                                claimInfo,
                                XplorationOntology.CLAIMCELLINFO);
                        send(msg);
                        System.out.println(getLocalName()+": CLAIMS CELL(S)");

                        //Updated claimed cells
                        map.addClaimedCells(map.getClaimableCells());

                        claimedCells = true;
                    } else {
                        // If no TerrainSimulator has been found, wait 5 seconds
                        System.out.println(getLocalName()+": NO RADIO CLAIM SERVICE FOUND!!!!");
                        doWait(5000);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public boolean done() {
                return claimedCells;
            }
        };
    }

    private Behaviour getMissionEndBehaviour() {
        return new WakerBehaviour(this, missionLength) {
            @Override
            public void onWake() {
                this.myAgent.doDelete();
            }
        };
    }

}