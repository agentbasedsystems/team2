package org.xploration.team2.company;

import jade.content.Concept;
import jade.content.ContentElement;
import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.*;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import jade.wrapper.ControllerException;
import org.xploration.ontology.*;
import org.xploration.team2.common.MessageBuilder;
import org.xploration.team2.company.map.AgentMap;

import java.awt.*;


public class Capsule2 extends Agent {

	private final Team team = new Team();

	private Codec codec = new SLCodec();
	private Ontology ontology = XplorationOntology.getInstance();
	private int missionLength;
	private int radioRange;

	private Point location;
	private AgentMap map;

	protected void setup(){
		System.out.println(getLocalName()+ " has entered into the system");
		getContentManager().registerOntology(ontology);
		getContentManager().registerLanguage(codec);

		team.setTeamId(2);

		Object[] args = getArguments();
		location = new Point((int) args[0], (int) args[1]);
		map = new AgentMap((int) args[2], (int) args[3]);
		missionLength = ((int) args[4])*1000;
		radioRange = (int) args[5];
		System.out.println(getLocalName() + " Starting location: " + location.toString());
		System.out.println(getLocalName() + " Map dimensions: (" + map.getDimension().getHeight() + "; " + map.getDimension().getWidth() + ")");

		addBehaviour(getCapsuleRegistrationBehaviour());
		addBehaviour(ReleaseRovers(args));
		addBehaviour(getCellClaimListenerBehaviour());
		addBehaviour(getMissionEndBehaviour());
	}

	private Behaviour getCapsuleRegistrationBehaviour() {
		return new SimpleBehaviour(this) {

			private boolean registeredWithPlatform = false;

			@Override
			public void action() {
				// Creates the description for the type of agent to be searched
				DFAgentDescription dfd = new DFAgentDescription();
				ServiceDescription sd = new ServiceDescription();
				sd.setType(XplorationOntology.CAPSULEREGISTRATIONSERVICE);
				dfd.addServices(sd);

				try {
					// It finds agents of the required type
					DFAgentDescription[] res = new DFAgentDescription[20];
					res = DFService.search(myAgent, dfd);

					// Gets the first occurrence, if there was success
					if (res.length > 0) {
						AID capsuleRegistrationAgent = res[0].getName();

						// Asks the estimation to the painter
						CapsuleRegistrationInfo regInfo = new CapsuleRegistrationInfo();
						regInfo.setTeam(team);
						Cell currCell = new Cell();
						currCell.setX(((Double)location.getX()).intValue());
						currCell.setY(((Double)location.getY()).intValue());
						regInfo.setCell(currCell);
						ACLMessage msg = MessageBuilder.getActionMessage(capsuleRegistrationAgent,
																		 ACLMessage.INFORM,
																		 regInfo,
																		 XplorationOntology.CAPSULEREGISTRATIONINFO);
						send(msg);
						System.out.println(getLocalName()+":  REGISTERS with the platform");

						registeredWithPlatform = true;
					} else {
						// If no RegistrationDesk has been found, wait 5 seconds
						doWait(1000);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			@Override
			public boolean done() {
				return registeredWithPlatform;
			}
		};
	}

	private Behaviour ReleaseRovers(Object[] args){
		return new OneShotBehaviour(this){
			public void action() {
				try {
					releaseRover(args);
					System.out.println(myAgent.getLocalName() + " is releasing a rover at "+ location.toString());
				} catch (ControllerException e) {
					e.printStackTrace();
				} finally{
					block();
				}
			}
		};
	}

	protected void releaseRover(Object[] args) throws ControllerException {

		ContainerController cc = getContainerController();
		AgentController ac = cc.createNewAgent("Rover2", "org.xploration.team2.company.Rover2", args);
		ac.start();
	}

	private Behaviour getCellClaimListenerBehaviour() {
		return new CyclicBehaviour(this) {

			@Override
			public void action() {
				// Waits for broadcasts
				ACLMessage msg = receive(
					MessageTemplate.and(
						MessageTemplate.MatchProtocol(XplorationOntology.CLAIMCELLINFO),
						MessageTemplate.and(
							MessageTemplate.MatchLanguage(codec.getName()),
							MessageTemplate.and(
								MessageTemplate.MatchOntology(ontology.getName()),
								MessageTemplate.MatchPerformative(ACLMessage.INFORM)
							)
						)
					)
				);
				if (msg != null) {
					System.out.println(getLocalName()+": RECEIVED CLAIM INFO");

					ContentElement ce;
					try {
						ce = getContentManager().extractContent(msg);

						if (ce instanceof Action) {
							Action agAction = (Action) ce;
							Concept conc = agAction.getAction();

							if (conc instanceof ClaimCellInfo) {

								ClaimCellInfo claimInfo = (ClaimCellInfo) conc;

								// Creates the description for the type of agent to be searched
								DFAgentDescription dfd = new DFAgentDescription();
								ServiceDescription sd = new ServiceDescription();
								sd.setType(XplorationOntology.SPACECRAFTCLAIMSERVICE);
								dfd.addServices(sd);

								try {
									// It finds agents of the required type
									DFAgentDescription[] res = new DFAgentDescription[20];
									res = DFService.search(myAgent, dfd);

									// Gets the first occurrence, if there was success
									if (res.length > 0) {
										AID spacecraftClaimAgent = res[0].getName();

										ACLMessage forwardMsg = MessageBuilder.getActionMessage(spacecraftClaimAgent,
												ACLMessage.INFORM,
												claimInfo,
												XplorationOntology.CLAIMCELLINFO);
										send(forwardMsg);
										System.out.println(getLocalName()+": CLAIMS CELL(S)");

									} else {
										// If no TerrainSimulator has been found, wait 5 seconds
										System.out.println(getLocalName()+": NO SPACECRAFT CLAIM SERVICE FOUND! :(");
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}
					} catch (Codec.CodecException | OntologyException e) {
						e.printStackTrace();
						System.out.println(myAgent.getLocalName()+": INVALID CELL CLAIM INFORM!");
					}
				}
			}

		};
	}

	private Behaviour getMissionEndBehaviour() {
		return new WakerBehaviour(this, missionLength) {
			@Override
			public void onWake() {
				this.myAgent.doDelete();
			}
		};
	}

}